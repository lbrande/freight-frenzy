# Freight Frenzy

Load the containers onto the ships as fast as possible.

Made for Ludum Dare 51 in Godot.

Playable at [https://lbrande.itch.io/freight-frenzy](https://lbrande.itch.io/freight-frenzy).

![Screenshot_1.png](images/Screenshot_1.png)

![Screenshot_2.png](images/Screenshot_2.png)

![Screenshot_3.png](images/Screenshot_3.png)
