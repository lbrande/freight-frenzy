extends CanvasLayer

const MAIN_SCENE := preload("res://menu.tscn")


func _ready() -> void:
	$AnimationPlayer.play("Splash")


func _input(event: InputEvent) -> void:
	if event.is_pressed():
		get_tree().change_scene_to(MAIN_SCENE)


func _on_AnimationPlayer_animation_finished(_anim_name: String) -> void:
	get_tree().change_scene_to(MAIN_SCENE)
