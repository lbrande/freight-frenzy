class_name Truck
extends Node2D

const SPAWN_Y := -152.0
const FINAL_Y := 302.0
const SPEED := FINAL_Y - SPAWN_Y

onready var world := get_parent()

var _tetromino: Tetromino
var _velocity := Vector2.ZERO


func _ready():
	position.y = SPAWN_Y
	randomize()
	_reset()
	_drive_forward()


func _physics_process(delta):
	if world.game_over:
		return
	position += _velocity * delta
	if position.y >= FINAL_Y:
		position.y = FINAL_Y
		_stop()
	if position.y <= SPAWN_Y:
		position.y = SPAWN_Y
		_reset()
		_drive_forward()


func _on_Tetromino_input_event(_viewport: Node, event: InputEvent, _shape_idx: int):
	if Utils.is_left_mouse_button_pressed(event):
		if world.grab_tetromino(_tetromino):
			_tetromino.disconnect("input_event", self, "_on_Tetromino_input_event")
			_drive_backwards()


func _reset():
	_tetromino = world.next_tetromino()
	_tetromino.connect("input_event", self, "_on_Tetromino_input_event")
	add_child(_tetromino)


func _drive_forward():
	_velocity = Vector2(0, SPEED)


func _stop():
	_velocity = Vector2.ZERO


func _drive_backwards():
	_velocity = Vector2(0, -SPEED)
