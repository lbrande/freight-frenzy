extends Node

const SQUARE_SIZE := 48.0


func is_left_mouse_button_pressed(event: InputEvent):
	return (event is InputEventMouseButton
		and event.button_index == BUTTON_LEFT
		and event.is_pressed())
