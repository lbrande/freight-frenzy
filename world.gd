extends Node2D

const TETROMINOES := [
	preload("res://tetrominoes/straight.tscn"),
	preload("res://tetrominoes/square.tscn"),
	preload("res://tetrominoes/dash.tscn"),
	preload("res://tetrominoes/cross.tscn"),
	preload("res://tetrominoes/bend.tscn"),
]

const BOAT := preload("res://boat.tscn")
const TRUCK := preload("res://truck.tscn")

onready var crate_sprites := [
	$CanvasLayer/TextureRect2,
	$CanvasLayer/TextureRect3,
	$CanvasLayer/TextureRect4,
	$CanvasLayer/TextureRect5,
	$CanvasLayer/TextureRect6,
]

onready var score_label: Label = $CanvasLayer/Label
onready var game_over_label: Label = $CanvasLayer/Label2
onready var crates := crate_sprites.size()

var score := 0
var game_over := false

var _boats := []
var _next_tetromino: int
var _tetromino: Tetromino


func _ready():
	_next_tetromino = rand_range(0, TETROMINOES.size())
	_spawn_boat(-360, 360, 560, Vector2(1, 0))
	_spawn_truck(400)
	_spawn_truck(560)
	_spawn_truck(720)
	_spawn_truck(880)
	yield(get_tree().create_timer(9.0), "timeout")
	_spawn_boat(1640, 920, 560, Vector2(-1, 0))


func _process(_delta):
	score_label.text = str(score)
	if _tetromino != null:
		_tetromino.position = get_local_mouse_position()
	for boat in _boats:
		boat.indicate_tetromino(_tetromino)


func _unhandled_input(event: InputEvent):
	if _tetromino != null and !game_over:
		if event.is_action_pressed("rotate_left"):
			_tetromino.rotate(- PI / 2)
		elif event.is_action_pressed("rotate_right"):
			_tetromino.rotate(PI / 2)
		elif event.is_action_pressed("flip"):
			_tetromino.scale.x = - _tetromino.scale.x
		elif Utils.is_left_mouse_button_pressed(event):
			for boat in _boats:
				if boat.can_place_tetromino(_tetromino):
					boat.place_tetromino(_tetromino)
					_tetromino = null
					break
	elif game_over and Utils.is_left_mouse_button_pressed(event):
		get_tree().change_scene("res://menu.tscn")

	
func next_tetromino() -> Tetromino:
	var tetromino := _next_tetromino
	while _next_tetromino == tetromino:
		_next_tetromino = rand_range(0, TETROMINOES.size())
	return TETROMINOES[tetromino].instance()


func grab_tetromino(tetromino: Tetromino):
	if _tetromino == null and !game_over:
		_tetromino = tetromino
		_tetromino.get_parent().remove_child(_tetromino)
		add_child(_tetromino)
		return true
	return false


func set_game_over():
	game_over_label.text = ("Game Over\nScore: " + str(score) + "\nClick to continue")
	game_over_label.show()
	yield(get_tree().create_timer(1.0), "timeout")
	game_over = true
	if _tetromino != null:
		remove_child(_tetromino)
		_tetromino = null


func _spawn_boat(spawn_x: float, final_x: float, y: float, direction: Vector2):
	var boat: Boat = BOAT.instance()
	boat.spawn_x = spawn_x
	boat.position.x = spawn_x
	boat.final_x = final_x
	boat.position.y = y
	boat.direction = direction
	_boats.append(boat)
	add_child(boat)
	boat.start()


func _spawn_truck(x: float):
	var truck: Truck = TRUCK.instance()
	truck.position.x = x
	add_child(truck)
