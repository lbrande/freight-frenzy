class_name Boat
extends Area2D

const CRATE := preload("res://tetrominoes/crate.tscn")

const WIDTH := 5
const HEIGHT := 5

onready var world := get_parent()
onready var sprite := $Sprite
onready var tile_map: TileMap = $TileMap
onready var timer: Timer = $Timer
onready var score_label: Label = $Label
onready var time_indicator: AnimatedSprite = $AnimatedSprite
onready var gain_points: AudioStreamPlayer2D = $AudioStreamPlayer2D
onready var lose_crate: AudioStreamPlayer2D = $AudioStreamPlayer2D2

var spawn_x: float
var final_x: float
var speed: float
var direction: Vector2

var _empty_cells := []
var _tetrominoes := []
var _velocity := Vector2.ZERO


func _ready():
	randomize()
	_setup_empty_cells()
	_setup()


func _process(_delta):
	score_label.text = str(_score())
	if world.game_over:
		return
	if timer.is_stopped() && _velocity.dot(direction) > 0:
		time_indicator.frame = 0
	else:
		time_indicator.frame = 16 - ceil(timer.time_left)


func _physics_process(delta):
	if world.game_over:
		return
	position += _velocity * delta
	if position.x * direction.x >= final_x * direction.x:
		if timer.is_stopped():
			timer.start()
		position.x = final_x
		_stop()
	if position.x * direction.x <= spawn_x * direction.x:
		position.x = spawn_x
		_reset()
		_drive_forward()


func _on_Timer_timeout():
	_drive_backwards()


func start():
	speed = (final_x - spawn_x) * direction.x
	sprite.scale.x *= direction.x
	score_label.rect_position.x *= direction.x
	time_indicator.position.x *= direction.x
	if direction.x < 0:
		score_label.rect_position.x -= score_label.rect_size.x
	_drive_forward()


func indicate_tetromino(tetromino: Tetromino):
	for x in range(0, WIDTH):
		for y in range(0, HEIGHT):
			if _is_free(x, y):
				tile_map.set_cell(x, y, 6)
	if tetromino == null:
		return
	if can_place_tetromino(tetromino):
		for cell in _covered_cells(tetromino):
			if _is_free(cell.x, cell.y):
				tile_map.set_cell(cell.x, cell.y, 7)
	else:
		for cell in _covered_cells(tetromino):
			if _is_free(cell.x, cell.y):
				tile_map.set_cell(cell.x, cell.y, 8)


func can_place_tetromino(tetromino: Tetromino):
	for cell in _covered_cells(tetromino):
		if !_is_free(cell.x, cell.y):
			return false
	return true


func place_tetromino(tetromino: Tetromino):
	for cell in _covered_cells(tetromino):
		if _is_free(cell.x, cell.y):
			_empty_cells[cell.x][cell.y] = false
	tetromino.position = _adjusted_position(tetromino)
	tetromino.get_parent().remove_child(tetromino)
	_tetrominoes.append(tetromino)
	add_child(tetromino)


func _reset():
	var score := _score()
	if score > 0:
		world.score += score
		gain_points.play()
	else:
		world.crates -= 1
		world.crate_sprites[world.crates].hide()
		if world.crates == 0:
			world.set_game_over()
		lose_crate.play()
	for tetromino in _tetrominoes:
		remove_child(tetromino)
	_tetrominoes = []
	_setup()


func _setup():
	_reset_empty_cells()
	_setup_crates()


func _drive_forward():
	_velocity = direction * speed


func _stop():
	_velocity = Vector2.ZERO


func _drive_backwards():
	_velocity = direction * -speed


func _adjusted_position(tetromino: Tetromino):
	var x_min := INF
	var x_max := -INF
	var y_min := INF
	var y_max := -INF
	for cell in _covered_cells(tetromino):
		x_min = min(cell.x, x_min)
		x_max = max(cell.x, x_max)
		y_min = min(cell.y, y_min)
		y_max = max(cell.y, y_max)
	return to_local(tile_map.to_global(
		tile_map.map_to_world(Vector2(x_min + x_max + 1, y_min + y_max + 1)) / 2))


func _covered_cells(tetromino: Tetromino):
	var tetromino_map := tetromino.tile_map
	var cells := []
	for cell in tetromino_map.get_used_cells():
		cells.append(tile_map.world_to_map(tile_map.to_local(tetromino_map.to_global(
			tetromino_map.map_to_world(cell) + tile_map.cell_size / 2))))
	return cells


func _setup_empty_cells():
	_empty_cells = []
	for x in range(0, WIDTH):
		_empty_cells.append([])
		for _y in range(0, HEIGHT):
			_empty_cells[x].append(true)


func _reset_empty_cells():
	for x in range(0, WIDTH):
		for y in range(0, HEIGHT):
			_empty_cells[x][y] = true


func _setup_crates():
	if randf() < 0.5:
		for x in range(0, WIDTH):
			var y := int(rand_range(0, HEIGHT))
			while _is_diagonal_to_any(x, y) or _num_on_row(y) > 1:
				y = int(rand_range(0, HEIGHT))
			_empty_cells[x][y] = false
			var crate: Tetromino = CRATE.instance()
			crate.position = to_local(tile_map.to_global(
				tile_map.map_to_world(Vector2(x, y)) + tile_map.cell_size / 2))
			_tetrominoes.append(crate)
			add_child(crate)
	else:
		for y in range(0, HEIGHT):
			var x := int(rand_range(0, WIDTH))
			while _is_diagonal_to_any(x, y) or _num_on_column(x) > 1:
				x = int(rand_range(0, WIDTH))
			_empty_cells[x][y] = false
			var crate: Tetromino = CRATE.instance()
			crate.position = to_local(tile_map.to_global(
				tile_map.map_to_world(Vector2(x, y)) + tile_map.cell_size / 2))
			_tetrominoes.append(crate)
			add_child(crate)


func _score() -> int:
	if _num_used() == WIDTH * HEIGHT:
		return 300
	elif _num_used() >= WIDTH * HEIGHT - 4:
		match _num_holes():
			1: return 75
			2: return 25
			3: return 10
			_: return 5
	else:
		return 0


func _num_used():
	var used := 0
	for tetromino in _tetrominoes:
		used += tetromino.tile_map.get_used_cells().size()
	return used


func _num_holes():
	var holes := 0
	for x in range(0, WIDTH):
		for y in range(0, HEIGHT):
			if _is_free(x, y) and !_is_free(x - 1, y) and !_is_free(x, y + 1):
				holes += 1
	return holes


func _is_diagonal_to_any(x: int, y: int):
	return (_is_full(x - 1, y - 1) or _is_full(x - 1, y + 1)
		or _is_full(x + 1, y - 1) or _is_full(x + 1, y + 1))


func _num_on_row(y: int):
	var num := 0
	for x in range(0, WIDTH):
		num += int(_is_full(x, y))
	return num


func _num_on_column(x: int):
	var num := 0
	for y in range(0, HEIGHT):
		num += int(_is_full(x, y))
	return num


func _is_free(x: int, y: int):
	return x >= 0 and x < WIDTH and y >= 0 and y < HEIGHT and _empty_cells[x][y]


func _is_full(x: int, y: int):
	return x >= 0 and x < WIDTH and y >= 0 and y < HEIGHT and !_empty_cells[x][y]
